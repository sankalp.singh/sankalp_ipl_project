function superOverBestEconomy(deliveryData) {
	const superDeliveryData = [];
	for (let items of deliveryData) {
		if (parseInt(items.is_super_over)) {
			superDeliveryData.push(items);
		}
	}

	// console.log(superDeliveryData);
	let bowlerBallCount = {};
	for (let items of superDeliveryData) {
		if (items.wide_runs === "0" && items.noball_runs === "0") {
			if (bowlerBallCount[items.bowler]) {
				bowlerBallCount[items.bowler]++;
			} else {
				bowlerBallCount[items.bowler] = 1;
			}
		}
	}
	// console.log(bowlerBallCount);
	let bowlerOvers = {};
	for (let bowlers in bowlerBallCount) {
		bowlerOvers[bowlers] =
			bowlerBallCount[bowlers] / 6 + (bowlerBallCount[bowlers] % 6) / 10;
	}

	let bowlerRuns = {};
	for (let items of superDeliveryData) {
		if (bowlerRuns[items.bowler]) {
			bowlerRuns[items.bowler] +=
				parseInt(items.total_runs) -
				(parseInt(items.bye_runs) +
					parseInt(items.legbye_runs) +
					parseInt(items.penalty_runs));
		} else {
			bowlerRuns[items.bowler] =
				parseInt(items.total_runs) -
				(parseInt(items.bye_runs) +
					parseInt(items.legbye_runs) +
					parseInt(items.penalty_runs));
		}
	}
	let economies = {};
	for (bowlers in bowlerOvers) {
		economies[bowlers] = bowlerRuns[bowlers] / bowlerOvers[bowlers];
	}
	const economyArr = Object.entries(economies);
	economyArr.sort((a, b) => a[1] - b[1]);
	let topEconomy = {};

	topEconomy[economyArr[0][0]] = economyArr[0][1];

	return topEconomy;
}
module.exports = superOverBestEconomy;
