const fs = require("fs");
const csv = require("csv-parser");
const matchPlayedPerYear = require("./matchesPlayedPerYear");
const matchWonPerTeam = require("./matchWonPerTeam");
const extraRunsPerTeam = require("./extraRunsPerTeam");
const topEconomicalBowler = require("./topEconomicalBowler");
const winTossWinMatch = require("./winTossWinMatch");
const playerOfYearPerYear = require("./playerOfYearPerYear");
const playerDismissedByOtherPlayers = require("./playerDismissedByOtherPlayer");
const strikeRatePerSeason = require("./strikeRatePerSeason");
const superOverBestEconomy = require("./superOverBestEconomy");

const deliveriesPath = "../data/deliveries.csv";
const matchesPath = "../data/matches.csv";
const outputPath = "../public/output/";

const matchResults = [];
const deliveryResults = [];
fs.createReadStream(matchesPath)
	.pipe(csv())
	.on("data", (data) => matchResults.push(data))
	.on("end", () => {
		const matchPerYearData = JSON.stringify(matchPlayedPerYear(matchResults));
		outputFile(matchPerYearData, "matchesPerYear");
		const matchWonPerTeamData = JSON.stringify(matchWonPerTeam(matchResults));
		outputFile(matchWonPerTeamData, "matchWonPerTeam");
		fs.createReadStream(deliveriesPath)
			.pipe(csv())
			.on("data", (data) => {
				deliveryResults.push(data);
			})
			.on("end", () => {
				const extraRunsPerTeamData = JSON.stringify(
					extraRunsPerTeam(matchResults, deliveryResults)
				);
				outputFile(extraRunsPerTeamData, "extraRunsPerTeam");
				const topEconomicalBowlerData = JSON.stringify(
					topEconomicalBowler(matchResults, deliveryResults)
				);
				outputFile(topEconomicalBowlerData, "topEconomicalBowlers");
				const winTossWinMatchData = JSON.stringify(
					winTossWinMatch(matchResults)
				);
				outputFile(winTossWinMatchData, "teamMostWinMatchWinToss");
				// console.log(winTossWinMatchData);
				const playerOfYearPerYearData = JSON.stringify(
					playerOfYearPerYear(matchResults)
				);
				outputFile(playerOfYearPerYearData, "playerOfMatchPerYear");
				// console.log(playerOfYearPerYearData);
				const strikeRatePerSeasonData = JSON.stringify(
					strikeRatePerSeason(matchResults, deliveryResults)
				);
				outputFile(strikeRatePerSeasonData, "strikeRatePerSeason");
				// console.log(strikeRatePerSeasonData);
				const playerDismissedByOtherPlayerData = JSON.stringify(
					playerDismissedByOtherPlayers(matchResults, deliveryResults)
				);
				outputFile(
					playerDismissedByOtherPlayerData,
					"playerDismissedByOtherPlayer"
				);
				// console.log(playerDismissedByOtherPlayerData);
				const superOverBestEconomyData = JSON.stringify(
					superOverBestEconomy(deliveryResults)
				);
				outputFile(superOverBestEconomyData, "superOverBestEconomy");
				// console.log(superOverBestEconomyData);
			});
	});

function outputFile(iplData, fileName) {
	fs.writeFile(outputPath + fileName + ".json", iplData, (err) => {
		if (err) {
			throw err;
		}
	});
}
