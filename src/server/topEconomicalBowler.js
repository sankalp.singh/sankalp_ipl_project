function topEconomicalBowler(matchData, deliveryData) {
	let matchId = [];
	for (let index = 0; index < matchData.length; index++) {
		if (matchData[index].season === "2015") {
			matchId.push(matchData[index].id);
		}
	}
	let bowlerRuns = {};
	for (let indexId = 0; indexId < matchId.length; indexId++) {
		for (let index = 0; index < deliveryData.length; index++) {
			if (
				parseInt(deliveryData[index].match_id) === parseInt(matchId[indexId])
			) {
				if (bowlerRuns[deliveryData[index].bowler]) {
					bowlerRuns[deliveryData[index].bowler] +=
						parseInt(deliveryData[index].total_runs) -
						(parseInt(deliveryData[index].bye_runs) +
							parseInt(deliveryData[index].legbye_runs) +
							parseInt(deliveryData[index].penalty_runs));
				} else {
					bowlerRuns[deliveryData[index].bowler] =
						parseInt(deliveryData[index].total_runs) -
						(parseInt(deliveryData[index].bye_runs) +
							parseInt(deliveryData[index].legbye_runs) +
							parseInt(deliveryData[index].penalty_runs));
				}
			}
		}
	}
	let bowlerBalls = {};

	for (let indexId = 0; indexId < matchId.length; indexId++) {
		for (let index = 0; index < deliveryData.length; index++) {
			if (
				parseInt(deliveryData[index].match_id) === parseInt(matchId[indexId])
			) {
				if (
					deliveryData[index].noball_runs === "0" &&
					deliveryData[index].wide_runs === "0"
				) {
					if (bowlerBalls[deliveryData[index].bowler]) {
						bowlerBalls[deliveryData[index].bowler]++;
					} else {
						bowlerBalls[deliveryData[index].bowler] = 1;
					}
				}
			}
		}
	}
	let bowlerOvers = {};
	for (bowlers in bowlerBalls) {
		bowlerOvers[bowlers] =
			bowlerBalls[bowlers] / 6 + (bowlerBalls[bowlers] % 6) / 10;
	}
	let economies = {};
	for (bowlers in bowlerOvers) {
		economies[bowlers] = bowlerRuns[bowlers] / bowlerOvers[bowlers];
	}
	const economyArr = Object.entries(economies);
	economyArr.sort((a, b) => a[1] - b[1]);
	let topEconomy = {};
	for (let index = 0; index < 10; index++) {
		topEconomy[economyArr[index][0]] = economyArr[index][1];
	}
	return topEconomy;
}
module.exports = topEconomicalBowler;
