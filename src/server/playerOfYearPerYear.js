function playerOfYearPerYear(matchData) {
	const playerOfYearPerYearData = {};
	for (let index = 0; index < matchData.length; index++) {
		if (playerOfYearPerYearData[matchData[index].season]) {
			if (
				playerOfYearPerYearData[matchData[index].season][
					matchData[index].player_of_match
				]
			) {
				playerOfYearPerYearData[matchData[index].season][
					matchData[index].player_of_match
				] += 1;
			} else {
				playerOfYearPerYearData[matchData[index].season][
					matchData[index].player_of_match
				] = 1;
			}
		} else {
			playerOfYearPerYearData[matchData[index].season] = {};
			playerOfYearPerYearData[matchData[index].season][
				matchData[index].player_of_match
			] = 1;
		}
	}
	let result = {};
	for (let item of matchData) {
		let playerOfMatch = [];
		for (let x in playerOfYearPerYearData[item.season]) {
			playerOfMatch.push([x, playerOfYearPerYearData[item.season][x]]);
		}
		playerOfMatch.sort(function (firstValue, secondValue) {
			return firstValue[1] - secondValue[1];
		});
		result[item.season] = playerOfMatch[playerOfMatch.length - 1][0];
	}
	return result;
}
module.exports = playerOfYearPerYear;
