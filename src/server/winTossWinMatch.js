function winTossWinMatch(matchData) {
	const winTossWinMatchData = {};
	for (let match = 0; match < matchData.length; match++) {
		if (matchData[match].toss_winner === matchData[match].winner) {
			if (winTossWinMatchData[matchData[match].toss_winner]) {
				winTossWinMatchData[matchData[match].toss_winner] += 1;
			} else {
				winTossWinMatchData[matchData[match].toss_winner] = 1;
			}
		}
	}
	return winTossWinMatchData;
}
module.exports = winTossWinMatch;
