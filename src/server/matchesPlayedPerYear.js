function matchesPlayedPerYear(matchData) {
	const matchPeryearData = matchData.reduce(function (acc, curr) {
		if (acc[curr.season]) {
			acc[curr.season] += 1;
		} else {
			acc[curr.season] = 1;
		}
		return acc;
	}, {});
	return matchPeryearData;
}
module.exports = matchesPlayedPerYear;
