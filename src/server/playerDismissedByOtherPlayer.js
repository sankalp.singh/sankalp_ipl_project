//Find the highest number of times one player has been dismissed by another player
function playerDismissedByOtherPlayers(matchData, deliveryData) {
	let dismissedObj = {};
	for (let items of deliveryData) {
		if (items.player_dismissed !== "") {
			if (items.dismissal_kind !== "run out") {
				if (dismissedObj[items.player_dismissed]) {
					dismissedObj[items.player_dismissed].push(items.bowler);
				} else {
					dismissedObj[items.player_dismissed] = [];
					dismissedObj[items.player_dismissed].push(items.bowler);
				}
			}
		}
	}
	let result = {};
	for (let items in dismissedObj) {
		result[items] = {};
		let bowlers = dismissedObj[items];
		let bowlerFrequency = {};
		for (let index = 0; index < bowlers.length; index++) {
			if (bowlerFrequency[bowlers[index]]) {
				bowlerFrequency[bowlers[index]] += 1;
			} else {
				bowlerFrequency[bowlers[index]] = 1;
			}
		}
		const bowlerArr = Object.entries(bowlerFrequency);
		bowlerArr.sort((a, b) => b[1] - a[1]);
		let maxBowler = {};
		maxBowler[bowlerArr[0][0]] = bowlerArr[0][1];
		result[items] = maxBowler;
	}
	return result;
}
module.exports = playerDismissedByOtherPlayers;
