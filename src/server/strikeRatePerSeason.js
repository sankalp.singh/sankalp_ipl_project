function strikeRatePerSeason(matchData, deliveryData) {
	let runsScored = {};
	for (let item of matchData) {
		for (let delivery of deliveryData) {
			if (item.id === delivery.match_id) {
				if (runsScored[item.season]) {
					if (runsScored[item.season][delivery.batsman]) {
						runsScored[item.season][delivery.batsman] += parseInt(
							delivery.batsman_runs
						);
					} else {
						runsScored[item.season][delivery.batsman] = parseInt(
							delivery.batsman_runs
						);
					}
				} else {
					runsScored[item.season] = {};
					runsScored[item.season][delivery.batsman_runs] = parseInt(
						delivery.batsman_runs
					);
				}
			}
		}
	}
	let ballsPlayed = {};

	for (let balls of matchData) {
		for (let delivery of deliveryData) {
			if (balls.id === delivery.match_id) {
				if (ballsPlayed[balls.season]) {
					if (ballsPlayed[balls.season][delivery.batsman]) {
						if (
							deliveryData[balls.wide_runs] !== "0" &&
							deliveryData[balls.noball_runs] !== "0"
						) {
							ballsPlayed[balls.season][delivery.batsman] += 1;
						}
					} else {
						if (
							deliveryData[balls.wide_runs] !== "0" &&
							deliveryData[balls.noball_runs] !== "0"
						) {
							ballsPlayed[balls.season][delivery.batsman] = 1;
						}
					}
				} else {
					ballsPlayed[balls.season] = {};
					ballsPlayed[balls.season][delivery.batsman] = 1;
				}
			}
		}
	}
	let bestStrikeRate = {};

	for (let item of matchData) {
		for (let delivery of deliveryData) {
			if (item.id === delivery.match_id) {
				if (bestStrikeRate[item.season]) {
					if (bestStrikeRate[item.season][delivery.batsman]) {
						bestStrikeRate[item.season][delivery.batsman] =
							(runsScored[item.season][delivery.batsman] /
								ballsPlayed[item.season][delivery.batsman]) *
							100;
					} else {
						bestStrikeRate[item.season][delivery.batsman] = {};
					}
				} else {
					bestStrikeRate[item.season] = {};
				}
			}
		}
	}
	return bestStrikeRate;
}
module.exports = strikeRatePerSeason;
