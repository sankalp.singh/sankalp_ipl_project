function extraRunsPerTeam(matchData, deliveryData) {
	let matchId = [];
	for (let index = 0; index < matchData.length; index++) {
		if (matchData[index].season === "2016") {
			matchId.push(matchData[index].id);
		}
	}
	let extraRunsPerTeamData = {};
	for (let index = 0; index < matchId.length; index++) {
		for (items of deliveryData) {
			if (matchId[index] === items.match_id) {
				if (extraRunsPerTeamData[items.bowling_team]) {
					extraRunsPerTeamData[items.bowling_team] += parseInt(
						items.extra_runs
					);
				} else {
					extraRunsPerTeamData[items.bowling_team] = parseInt(items.extra_runs);
				}
			}
		}
	}

	return extraRunsPerTeamData;
}
module.exports = extraRunsPerTeam;
