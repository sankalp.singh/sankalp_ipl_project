function matchWonPerTeam(matchData) {
	const matchWonPerTeamData = matchData.reduce(function (acc, curr) {
		if (acc[curr.winner]) {
			acc[curr.winner] += 1;
		} else {
			acc[curr.winner] = 1;
		}
		return acc;
	}, {});
	return matchWonPerTeamData;
}
module.exports = matchWonPerTeam;
